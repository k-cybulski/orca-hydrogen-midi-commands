# Orca operators for Hydrogen's TR808EmulationKit
This is a list of IO operators for [Orca](https://100r.co/site/orca.html) that, if banged (with `*`) output midi codes mapped by the [virtual drum kit Hydrogen](http://hydrogen-music.org/) (see [mapping](http://hydrogen-music.org/documentation/manual/manual_en_chunked/ch04s03.html)) to sounds of the Roland TR-808 emulation kit that comes with Hydrogen.

These mappings were found more or less by brute force but a smarter person could (a) read the source code of Orca or (b) use qmidiroute to understand how the mapping from Orca commands to MIDI codes works.

In addition to the list below, you can find the file `codes.orca` which can just be opened up in Orca itself to get these commands loaded. Also, there is `live-example.orca` which will play all of them on repeat.

## Operators (short list)
```
:f3Ca3 # Kick long
:f3ca3 # Kick short
:f3Da3 # Snare 1
:f3da3 # Snare 2
:f3Ea3 # Clap
:f3ea3 # Tom low
:f3fa3 # Tom mid
:f3Ga3 # Tom hi
:f3ga3 # Closed hat
:f3Ha3 # Pedal hat
:f3aa3 # Open hat
:f3Ba3 # Cymbal
:f3ia3 # Shaker
:f3ja3 # Conga
:f3Ka3 # Clave       
:f3ka3 # Cowbell
```

## Operators (long list)
Commands which do not result in any sounds are uncommented. I only checked three octaves, since all of the necessary codes are here anyway.

```
:f3aa3 # Open hat
:f3Aa3 # Pedal hat
:f3ba3 # Shaker
:f3Ba3 # Cymbal
:f3ca3 # Kick short
:f3Ca3 # Kick long
:f3da3 # Snare 2
:f3Da3 # Snare 1
:f3ea3 # Tom low
:f3Ea3 # Clap
:f3fa3 # Tom mid
:f3Fa3 # Tom low
:f3ga3 # Closed hat
:f3Ga3 # Tom hi
:f3ha3 # Open hat
:f3Ha3 # Pedal hat
:f3ia3 # Shaker
:f3Ia3 # Cymbal
:f3ja3 # Conga
:f3Ja3 # Shaker
:f3ka3 # Cowbell
:f3Ka3 # Clave       
:f3la3
:f3La3
:f3ma3
:f3Ma3
:f3na3
:f3Na3
:f3oa3
:f3Oa3
:f3pa3
:f3Pa3
:f3qa3
:f3Qa3
:f3ra3
:f3Ra3
:f3sa3
:f3Sa3
:f3ta3
:f3Ta3
:f3ua3
:f3Ua3
:f3va3
:f3Va3
:f3wa3
:f3Wa3
:f3xa3
:f3Xa3
:f3ya3
:f3Ya3
:f3za3
:f3Za3

:f2aa3
:f2Aa3
:f2ba3 # Kick long
:f2Ba3
:f2ca3
:f2Ca3
:f2da3
:f2Da3
:f2ea3
:f2Ea3
:f2fa3
:f2Fa3
:f2ga3
:f2Ga3
:f2ha3
:f2Ha3
:f2ia3 # Kick long
:f2Ia3
:f2ja3 # Open hat     
:f2Ja3 # Pedal hat    
:f2ka3 # Shaker       
:f2Ka3 # Cymbal
:f2la3 # Kick short
:f2La3 # Kick long
:f2ma3 # Snare 2
:f2Ma3 # Snare 1
:f2na3 # Tom low
:f2Na3 # Clap
:f2oa3 # Tom mid
:f2Oa3 # Tom low
:f2pa3 # Closed hat
:f2Pa3 # Tom hi
:f2qa3 # Open hat
:f2Qa3 # Pedal hat
:f2ra3 # Shaker
:f2Ra3 # Cymbal
:f2sa3 # Conga
:f2Sa3 # Shaker
:f2ta3 # Cowbell
:f2Ta3 # Clave       
:f2ua3 
:f2Ua3 
:f2va3 
:f2Va3 
:f2wa3 
:f2Wa3 
:f2xa3 
:f2Xa3 
:f2ya3 
:f2Ya3 
:f2za3 
:f2Za3

:f1aa3
:f1Aa3
:f1ba3
:f1Ba3
:f1ca3
:f1Ca3
:f1da3
:f1Da3
:f1ea3
:f1Ea3
:f1fa3
:f1Fa3
:f1ga3
:f1Ga3
:f1ha3
:f1Ha3
:f1ia3
:f1Ia3
:f1ja3
:f1Ja3
:f1ka3
:f1Ka3
:f1la3
:f1La3
:f1ma3
:f1Ma3
:f1na3
:f1Na3
:f1oa3
:f1Oa3 # Nothing above this line
:f1pa3 # Kick long
:f1Pa3 # ---
:f1qa3 # Kick short
:f1Qa3 # Kick long
:f1ra3 # Snare 2
:f1Ra3 # Snare 1
:f1sa3 # Tom low
:f1Sa3 # Clap
:f1ta3 # Tom mid
:f1Ta3 # Tom low
:f1ua3 # Closed hat
:f1Ua3 # Tom hi
:f1va3 # Open hat
:f1Va3 # Pedal hat
:f1wa3 # Shaker
:f1Wa3 # Cymbal
:f1xa3 # Conga
:f1Xa3 # Shaker
:f1ya3 # Cowbell
:f1Ya3 # Clave
:f1za3 # Nothing below this line
:f1Za3

:f0aa3
:f0Aa3
:f0ba3
:f0Ba3
:f0ca3
:f0Ca3
:f0da3
:f0Da3
:f0ea3
:f0Ea3
:f0fa3
:f0Fa3
:f0ga3
:f0Ga3
:f0ha3
:f0Ha3
:f0ia3
:f0Ia3
:f0ja3
:f0Ja3
:f0ka3
:f0Ka3
:f0la3
:f0La3
:f0ma3
:f0Ma3
:f0na3
:f0Na3
:f0oa3
:f0Oa3 
:f0pa3 
:f0Pa3 
:f0qa3 
:f0Qa3 
:f0ra3 
:f0Ra3 
:f0sa3 
:f0Sa3 
:f0ta3 
:f0Ta3 
:f0ua3 
:f0Ua3 
:f0va3 
:f0Va3 
:f0wa3 
:f0Wa3 
:f0xa3 
:f0Xa3 
:f0ya3 
:f0Ya3 
:f0za3 
:f0Za3
```
